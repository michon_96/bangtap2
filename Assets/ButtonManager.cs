﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour
{
    public void ReturnMainMenu()
    {
        SceneManager.LoadScene("main_menu");
    }

    public void StartGameButton(string newGameLevel)
    {
        SceneManager.LoadScene(newGameLevel);
    }

    public void ExitGameButton()
    {
        Application.Quit();
    }

    public void PauseButton()
    {
        print("Paused");
        Time.timeScale = 0;

    }

    public void ResumeButton()
    {
        print("Resumed");
        Time.timeScale = 1;
			

    }
}
