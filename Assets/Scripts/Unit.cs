﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class Unit : MonoBehaviour
{

    private FieldOfView fov;
    private Light flashLight;
    private NavMeshAgent navMesh;
    private SphereCollider detectionSeeker;
    private ShowSakButton sakDetector;
    [SerializeField] string playerName;
    [SerializeField] GameObject unitMesh;
    [SerializeField] float hiderSpeed = 15.6f;
    [SerializeField] float seekerSpeed = 13.6f;
    [SerializeField] Color seekerColor = Color.red;
    [SerializeField] Color hiderColor = Color.grey;
    public UnitType playerType;
    private bool isVisibleToSeeker;

    void Start()
    {
        navMesh = this.GetComponent<NavMeshAgent>();
        flashLight = this.GetComponent<Light>();
        detectionSeeker = this.GetComponent<SphereCollider>();
        sakDetector = this.GetComponent<ShowSakButton>();
        fov = GetComponent<FieldOfView>();
        checkUnitType();
    }

    void FixedUpdate()
    {
        checkUnitType();
    }

    public void checkUnitType()
    {
        if (playerType.Equals(UnitType.Hider) && !this.tag.Equals("Hider"))
        {
            Debug.Log("changing to hider");
            changeToHider();
        }
        else if (playerType.Equals(UnitType.Seeker) && !this.tag.Equals("Seeker"))
        {
            Debug.Log("changing to seeker");
            changeToSeeker();
        }
    }

    public void changeToHider()
    {
        this.gameObject.tag = "Hider";
        flashLight.enabled = false;
        sakDetector.enabled = true;
        detectionSeeker.enabled = true;
        unitMesh.GetComponent<MeshRenderer>().material.color = hiderColor;
        navMesh.acceleration = hiderSpeed;
        navMesh.speed = hiderSpeed - 1f;
        fov.viewAngle = 360;
    }

    public void changeToSeeker()
    {
        this.gameObject.tag = "Seeker";
        unitMesh.GetComponent<MeshRenderer>().material.color = seekerColor;
        flashLight.enabled = true;
        sakDetector.enabled = false;
        detectionSeeker.enabled = false;
        navMesh.acceleration = seekerSpeed;
        navMesh.speed = seekerSpeed - 1;
        fov.viewAngle = 222;

    }

    public bool isSeeker()
    {
        if (this.playerType.Equals(UnitType.Seeker))
        {
            return false;
        }
        else
            return true;
    }

    public enum UnitType
    {
        Hider = 15,
        Seeker
    }
}
