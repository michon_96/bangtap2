﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float rayDistance = 10f;
    [SerializeField] float totaldistance = 3;
    [SerializeField] float rotationSpeed = 1;
    public GameObject pointer;
    public bool isSneaking;
    Animator anim;
    NavMeshAgent navMesh;
    Vector3 target;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        navMesh = this.GetComponent<NavMeshAgent>();
    }

    void changeTarget()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//draws a ray
        navMesh.SetDestination(ray.GetPoint(rayDistance));
        target = ray.GetPoint(rayDistance);
        pointer.transform.position = target;
    }

    void Update()
    {
        if (Input.GetMouseButton(0)) //tap to move
        {
            changeTarget();
        }
//        lookAtPointer(target);
        moveTowardsTarget();
    }

    void moveTowardsTarget()
    {
        if (navMesh.remainingDistance < totaldistance)
        {
            anim.SetTrigger("idle");
            pointer.SetActive(false);
            navMesh.isStopped = true;
        }
        else if (navMesh.remainingDistance > totaldistance || navMesh.remainingDistance <= 0.5f)// continue moving
        {
            anim.SetTrigger("walk");
            pointer.SetActive(true);
            navMesh.isStopped = false;
        } 
    }

    void lookAtPointer(Vector3 targetPoint)
    {
        Quaternion targetRotation = Quaternion.LookRotation(targetPoint - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
}
