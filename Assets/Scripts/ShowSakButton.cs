﻿using System.Collections.Generic;
﻿using System.Collections;
using UnityEngine;

public class ShowSakButton : MonoBehaviour
{
    bool sakButton = false;
    public GameObject button;
    public static GameObject enemy;

    void Start()
    {
        if (this.gameObject.tag.Equals("Seeker"))
        {
            this.enabled = false;
        }
    }

    private void OnTriggerStay(Collider other)
    {
        Debug.Log(other.tag);
        Debug.Log(other.gameObject.name + " entered");
        if (other.transform.parent.tag.Equals("Seeker"))
        {
            sakButton = true;
            button.gameObject.SetActive(true);
            enemy = other.gameObject;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other + "entered");
        if (other.tag.Equals("Player"))
        {
            sakButton = true;
            button.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        sakButton = false;
        button.gameObject.SetActive(false);
    }

    public void OnClick()
    {
        sakButton = false;
        button.gameObject.SetActive(false);
        Destroy(enemy.transform.parent.gameObject);
    }
}
