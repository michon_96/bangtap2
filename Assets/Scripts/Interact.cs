﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interact : MonoBehaviour
{
    // Update is called once per frame
    [SerializeField] float rayDistance = 10f;
    Vector3 target;
    RaycastHit hit;

    void Update()
    {
        pointAndClick();
    }

    public virtual void pointAndClick()
    {
        //overidable by playermovement and Bang
        if (Input.GetMouseButtonDown(0)) //tap to move
        {
            if (this.tag.Equals("Seeker"))
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//draws a ray
                if (Physics.Raycast(ray, out hit))
                {
                    GameObject parent = hit.transform.parent.gameObject;
                    Debug.Log("laser hit a tag:" + parent.tag);
                    Debug.DrawRay(Camera.main.transform.position, ray.GetPoint(rayDistance), Color.red);
                    if (parent.tag == "Hider")
                    {
                        Destroy(parent);
                    }
                }

            }
        }
    }
}
