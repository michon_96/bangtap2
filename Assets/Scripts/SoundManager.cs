﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SoundType
{
    UnitCallOut,
    Object,
    Environment
}

[System.Serializable]
public struct Sound
{
    public SoundType stype;
    public List<AudioClip> clips;
}

public class SoundManager : MonoBehaviour
{
    public List<Sound> soundList;

    public void playSound()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
		
    }
}
