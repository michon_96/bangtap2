﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class GameStats : MonoBehaviour
{

    [SerializeField] int totalDrugs = 0;
    [SerializeField] float timer;

    public int foundDrugs;
    public int casualties;
    //HUD
    public Text uiTimer;
    public Text uiDrugs;
    public Text uiCasualty;
    //Options
    public GameObject panelGameOver;
    public GameObject panelPause;
    public GameObject panelLvlComplete;

    public static GameStats instance;

    void Awake()
    {
        Time.timeScale = 1;
    }

    // Use this for initialization
    void Start()
    {
        instance = this;
        timer = 0;
        foundDrugs = 0;
        casualties = 0;
        uiDrugs.text = string.Format("Drugs Found: {0}/{1}", foundDrugs, totalDrugs);
        uiCasualty.text = string.Format("Casualties: {0}", casualties);

    }
    // Update is called once per frame
    void FixedUpdate()
    {
        timer += Time.fixedDeltaTime;
        uiTimer.text = string.Format("Time: {0:0}", timer);
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            PauseGame();
        }
    }

    public void drugFound()
    {
        foundDrugs++;
        if (foundDrugs >= totalDrugs)
        {
            LevelComplete();
        }
        uiDrugs.text = string.Format("Drugs Found: {0}/{1}", foundDrugs, totalDrugs);
    }

    public void AddCasualty()
    {
        casualties++;
        uiCasualty.text = string.Format("Casualties: {0}", casualties);
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        panelGameOver.SetActive(true);
    }

    public void PauseGame()
    {
        if (!panelPause.activeInHierarchy)
        {
            Time.timeScale = 0;
            panelPause.SetActive(true);    
        }
        else
        {
            Time.timeScale = 1;
            panelPause.SetActive(false); 
        }
    }

    public void LevelComplete()
    {
        Time.timeScale = 0;
        panelLvlComplete.SetActive(true);

    }

    public void calculateScore()
    {
        
    }
}
