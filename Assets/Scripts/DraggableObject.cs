﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DraggableObject : MonoBehaviour
{
    Ray objectRay;
    RaycastHit objectHit;
    RaycastHit groundHit;
    RaycastHit mousePosHit;

    public Transform objectTransform;
    public Transform mousePosMarker;
    public Transform myGround;
    public LayerMask moveableObject;
    public LayerMask ground;
    public Vector3 mousePosRelativeToGround;

    public float objectYOffsetFromGround = 0f;
    public float mousePosYOffsetFromGround = 0f;

    public bool grabbedMoveableObject = false;

    private void Start()
    {
        objectHit = new RaycastHit();
        groundHit = new RaycastHit();
        mousePosMarker.gameObject.SetActive(false);
    }

    private void Update()
    {
        objectRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(0))
        {
            FindAndGrabMoveableObject();
        }

        if (Input.GetMouseButtonUp(0))
        {
            DropMoveableObject();
        }

        grabbedMoveableObject = objectTransform != null;
        mousePosMarker.gameObject.SetActive(grabbedMoveableObject);

        if (grabbedMoveableObject)
        {
            TraceMousePosRelativeToGround();
        }
    }

    void FindAndGrabMoveableObject()
    {
        if (Physics.Raycast(objectRay, out objectHit, Mathf.Infinity, moveableObject))
        {
            objectTransform = objectHit.transform;
            objectTransform.GetComponent<Rigidbody>().isKinematic = true;
            FindGroundBelowMoveableObject();
        }
    }

    void FindGroundBelowMoveableObject()
    {
        if (Physics.Raycast(objectTransform.position, Vector3.down, out groundHit, Mathf.Infinity, ground))
        {
            myGround = groundHit.transform;
        }
    }

    void TraceMousePosRelativeToGround()
    {
        if (Physics.Raycast(objectRay, out mousePosHit, Mathf.Infinity, ground))
        {
            mousePosRelativeToGround = mousePosHit.point;
            objectTransform.position = new Vector3(mousePosRelativeToGround.x, mousePosRelativeToGround.y + objectYOffsetFromGround, mousePosRelativeToGround.z);
            mousePosMarker.position = new Vector3(mousePosRelativeToGround.x, mousePosRelativeToGround.y + mousePosYOffsetFromGround, mousePosRelativeToGround.z);
        }
    }

    void DropMoveableObject()
    {
        if (objectTransform != null)
        {
            objectTransform.GetComponent<Rigidbody>().isKinematic = false;
            objectTransform = null;
            myGround = null;
        }
    }
}
