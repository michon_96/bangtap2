﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekerBang : Interact
{
    [SerializeField] int pointerLength = 20;
    private Unit unit;

    // Use this for initialization
    void Start()
    {
        unit = this.GetComponent<Unit>();
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);//draws a ray
            Debug.DrawRay(Camera.main.transform.position, ray.GetPoint(pointerLength), Color.red);
//            navMesh.SetDestination(ray.GetPoint(rayDistance));
        }	
    }
}
