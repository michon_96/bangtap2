﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Scene : MonoBehaviour {

    public void Level()
    {
        SceneManager.LoadScene("Level_Test");
    }

    public void Menu()
    {
        SceneManager.LoadScene("Main Menu");
    }

    public void Lobby()
    {
        SceneManager.LoadScene("Lobby");
    }
}
